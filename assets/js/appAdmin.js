var io = require('socket.io-client');

import {getHost} from './common.js';

var socket = io.connect(getHost(),  {secure: true});
require('./jquery.form.min.js');

bindProposalEvent();
bindCancelOrder();

socket.on('refreshOrder', function () {
    var listIdsElement = document.getElementById('listIds');
    var listIds = listIdsElement.innerText.split(',');
    if (listIds[0] === "") {
        listIds = []
    }
    $.post({
        url: '/admin/refresh-open-ordre',
        data: {
            'listIds': listIds
        },
        success: function (data) {
            var table = document.getElementById('openOrders');
            data.forEach(function (element) {
                var newRow = table.insertRow(table.rows.length);
                var cell0 = newRow.insertCell(0);
                var cell1 = newRow.insertCell(1);
                var cell2 = newRow.insertCell(2);
                var cell3 = newRow.insertCell(3);
                var cell4 = newRow.insertCell(4);
                var cell5 = newRow.insertCell(5);
                var cell6 = newRow.insertCell(6);

                newRow.id = 'auth' + element.authorId + 'Ord' + element.id;
                cell0.innerHTML = element.id;
                cell1.innerHTML = element.full_name;
                cell2.innerHTML = element.amount;
                cell3.innerHTML = element.pair;
                cell4.innerHTML = element.type;
                cell5.innerHTML = "" +
                    "<form class='form proposal' action=\"/admin/proposal\" method=\"post\">\n" +
                    "<input type=\"text\" name=\"proposal\" required>\n" +
                    "<input type=\"hidden\" name=\"authorId\" value=\"" + element.authorId + "\">\n" +
                    "<input type=\"hidden\" name=\"orderId\" value=\"" + element.id + "\">\n" +
                    "<input class=\"btn btn-primary\" type=\"submit\" value=\"envoyer\">\n" +
                    "</form>";
                cell6.innerHTML = "<button class=\"cancelOrder btn btn-sm btn-danger\" data-author-id='"+element.authorId+"' data-order-id='"+element.id+"'><i class=\"fa fa-close\"></i></button>";

                //On met à jour la liste des ids existants
                if (listIdsElement.innerText === "") {
                    listIdsElement.innerHTML = element.id;
                }
                else {
                    listIdsElement.innerHTML = listIdsElement.innerText + "," + element.id;
                }

                bindProposalEvent();
                bindCancelOrder();
            });


        }
    })
});

socket.on('acceptedRefreshProposal', function (data) {
    var element = document.getElementById('auth' + data.authorId + 'Ord' + data.orderId);
    var cells =  element.getElementsByTagName('td');
    var proposalCell = cells.item(cells.length - 2);
    var cancelCell = cells.item(cells.length - 1);

    proposalCell.innerHTML = 'Proposal accepted';
    cancelCell.innerHTML = '';
});

socket.on('refusedRefreshProposal', function (data) {
    var element = document.getElementById('auth' + data.authorId + 'Ord' + data.orderId);
    var cells =  element.getElementsByTagName('td');
    var proposalCell = cells.item(cells.length - 2);
    var cancelCell = cells.item(cells.length - 1);

    proposalCell.innerHTML = 'Proposal was refused or not accepted, it will disapear';
    cancelCell.innerHTML = '';

    setTimeout(function () {
        element.parentNode.removeChild(element);
    }, 3000);
});

socket.on('cancelFromUserOrder', function (data) {
    console.log('test');
    var element = document.getElementById('auth' + data.authorId + 'Ord' + data.orderId);
    var cells =  element.getElementsByTagName('td');
    var proposalCell = cells.item(cells.length - 2);
    var cancelCell = cells.item(cells.length - 1);

    proposalCell.innerHTML = 'Order was canceled by user, it will disapear';
    cancelCell.innerHTML = '';

    setTimeout(function () {
        element.parentNode.removeChild(element);
    }, 3000);
});



function bindProposalEvent() {
    var classname = document.getElementsByClassName("proposal");

    Array.from(classname).forEach(function (element) {
        element.addEventListener('submit', function (event) {
            event.preventDefault();
            var authorId = element.elements['authorId'].value;
            var orderId = element.elements['orderId'].value;
            var proposalOption = {
                success: function (response, statusText, xhr) {
                    var element = document.getElementById('auth' + authorId + 'Ord' + orderId);
                    var cells = element.getElementsByTagName('td');
                    var proposalCell = cells.item(cells.length - 2);
                    socket.emit('isUserStillHere',  {authorId : authorId}, function (isHere){
                        if(!isHere){
                            proposalCell.innerHTML = 'User left the page so order has been auto cancel, it will disapear';

                            $.get({ url: '/admin/cancel-ordre/' + authorId});
                            setTimeout(function () {
                                element.parentNode.removeChild(element);
                            }, 3000);

                            return;
                        }

                        if (xhr.status !== 201) {
                            proposalCell.innerHTML = 'User refreshed page so order has been auto cancel, it will disapear';

                            setTimeout(function () {
                                element.parentNode.removeChild(element);
                            }, 3000);

                            return;
                        }

                        $.get({
                            url: '/admin/ordre',
                            success: function (response) {
                                document.getElementById('auth' + authorId + 'Ord' + orderId).innerHTML = $(response).find('#openOrders #auth' + authorId + 'Ord' + orderId).html();
                                bindCancelOrder();
                                socket.emit('proposal', authorId);
                                startCountDown(15, socket, authorId, orderId);
                            }
                        })
                    });
                },
            };
            $(this).ajaxSubmit(proposalOption);
        });
    });
}

function startCountDown(seconds, socket, authorId, orderId) {
    var timeleft = seconds;
    var countdown = setInterval(function () {
        var progressBar = document.getElementById("countdown" + authorId + 'Ord' + orderId);
        if (progressBar === null) {
            clearInterval(countdown);
            return;
        }
        progressBar.value = seconds - --timeleft;
        if (timeleft <= 0) {
            clearInterval(countdown);
            $.get({
                url: '/admin/cancel-ordre/' + authorId,
                success: function () {
                    var element = document.getElementById('auth' + authorId + 'Ord' + orderId);
                    var cells = element.getElementsByTagName('td');
                    var lastCell = cells.item(cells.length - 2);
                    lastCell.innerHTML = 'No answer during time, it will disapear';
                    setTimeout(function () {
                        element.parentNode.removeChild(element);
                    }, 3000);

                    socket.emit('cancelOrder', {authorId: authorId, type: 'countdown'});
                }
            });
        }
    }, 1000);
}

function bindCancelOrder() {
    var classname = document.getElementsByClassName("cancelOrder");

    Array.from(classname).forEach(function (element) {
        element.addEventListener('click', function () {
            var authorId = this.dataset.authorId;
            var orderId = this.dataset.orderId;
            $.get({
                url: '/admin/cancel-ordre/' + authorId,
                success: function () {
                    socket.emit('cancelOrder', {authorId: authorId, type: 'cancelAdmin'});

                    var element = document.getElementById('auth' + authorId + 'Ord' + orderId);
                    var cells = element.getElementsByTagName('td');
                    var lastCell = cells.item(cells.length - 2);
                    lastCell.innerHTML = 'You canceled this order, it will disapear';
                    setTimeout(function () {
                        element.parentNode.removeChild(element);
                    }, 3000);
                }
            });
        });
    });
}
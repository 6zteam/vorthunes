let stopCountdow = false;

export function getHost() {
    var nodeEnv = process.env.NODE_ENV;
    if(nodeEnv === 'production'){
        return 'http://18.223.121.160:8080'
    }

    return 'localhost:8080'
}

export function setStopCountdown(value){
    stopCountdow = value;
}

export function startCountDown(seconds){
    var timeleft = seconds;
    stopCountdow = false;
    var countdown = setInterval(function(){
        if(stopCountdow){
            clearInterval(countdown);
            return;
        }
        document.getElementById("countdown").value = seconds - --timeleft;
        if(timeleft <= 0){
            clearInterval(countdown);
        }
    },1000);
}
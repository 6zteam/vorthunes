var io = require('socket.io-client');

require('./jquery.form.min.js');

import {setStopCountdown, startCountDown, getHost} from './common.js';

var socket = io.connect(getHost(), { secure: true });
var userId = document.getElementById('userId').innerText;


socket.on('connect', function () {
    socket.emit('storeClientInfo', { customId:  userId });
});

addPreSubmitOnForm();
addEventOnCancelMyOrderButton();

function addEventOnCancelMyOrderButton(){
    var cancelMyOrderButton = document.getElementById('cancelMyOrder');
    if(cancelMyOrderButton){
        cancelMyOrderButton.addEventListener('click', function(){
            var orderId = $(this).data('order-id');
            $.get({
                url: '/cancel-open-order',
                success: function () {
                    socket.emit('cancelMyOrder', { authorId: userId, orderId: orderId });
                    document.getElementById('userOrdre').innerHTML = "" +
                        "<h4 class='text-center'>" +
                        "You canceled the proposal <br>" +
                        "<button class='btn btn-primary' id = 'retryOrdre'>Make a new one</button>" +
                        "</h4>";

                    addEventOnRetryOrdre();
                }
            })
        });
    }
}


function addPreSubmitOnForm() {
    var ordreForm = document.getElementById('ordre');
    if (ordreForm) {
        ordreForm.addEventListener('submit', function (event) {
            event.preventDefault();
            var orderOptions = {
                dataType: 'html',
                success: function (response) {
                    socket.emit('newOrder');
                    $('#userOrdre').fadeOut(function () {
                        $(this).html($(response).find('#userOrdre').html()).fadeIn();
                        addEventOnCancelMyOrderButton();
                    });
                }
            };


            $('#ordre').ajaxSubmit(orderOptions);
        });
    }
}

function addEventOnRetryOrdre() {
    var retryButton = document.getElementById('retryOrdre');
    retryButton.addEventListener('click', function () {
        $.get({
            url: '/order',
            success: function (response) {
                document.getElementById('userOrdre').innerHTML = $(response).find('#userOrdre').html();
                addPreSubmitOnForm();
            }
        })
    });
}

function addEvenOnAcceptProposal() {
    var acceptButton = document.getElementById('acceptProposal');
    acceptButton.addEventListener('click', function () {
        var orderId = $(this).data('order-id');
        $.get({
            url: '/accept-open-order',
            success: function () {
                socket.emit('acceptedProposal', { authorId: userId, orderId: orderId});
                setStopCountdown(true);
                document.getElementById('userOrdre').innerHTML = "" +
                    "<h4 class='text-center'>" +
                    "You accepted the proposal <br>" +
                    "<button class='btn btn-primary' id = 'retryOrdre'>Make a new one</button>" +
                    "</h4>";

                addEventOnRetryOrdre();
            }
        })
    });
}

function addEvenOnRefuseProposal() {
    var refuseButton = document.getElementById('refuseProposal');
    refuseButton.addEventListener('click', function () {
        var orderId = $(this).data('order-id');
        $.get({
            url: '/refuse-open-order',
            success: function () {
                socket.emit('refusedProposal', { authorId: userId, orderId: orderId});
                setStopCountdown(true);
                document.getElementById('userOrdre').innerHTML = "" +
                    "<h4 class='text-center'>" +
                    "You refused the proposal <br>" +
                    "<button class='btn btn-primary' id = 'retryOrdre'>Make a new one</button>" +
                    "</h4>";

                addEventOnRetryOrdre();
            }
        })
    });
}

socket.on('refreshProposal', function (authorId) {
    if (userId === authorId) {
        $.get({
            url: '/order',
            success: function (response) {
                document.getElementById('userOrdre').innerHTML = $(response).find('#userOrdre').html();
                startCountDown(15);
                addEvenOnAcceptProposal();
                addEvenOnRefuseProposal();
            }
        })
    }
});

socket.on('refreshCancelOrder', function (data) {
    if (userId === data.authorId) {
        setStopCountdown(true);
        var message = "Your order has been cancel by an admin <br>";

        if (data.type === 'countdown') {
            message = "Times of 15 seconds over, your order has been cancel<br>";
        }

        document.getElementById('userOrdre').innerHTML = "" +
            "<h4  class='text-center'>" +
            message +
            "<button class='btn btn-primary' id = 'retryOrdre'>Make a new one</button>" +
            "</h4>";

        addEventOnRetryOrdre();
    }
});
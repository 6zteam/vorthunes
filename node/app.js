'use strict';

var https = require('https');
const fs = require('fs');
process.env["NODE_CONFIG_DIR"] = __dirname + "/config";

var config = require('config');

// Chargement du fichier index.html affiché au client
var server = https.createServer(
    {
        key: fs.readFileSync('/etc/letsencrypt/live/otc.woorton.com/privkey.pem'),
        cert: fs.readFileSync('/etc/letsencrypt/live/otc.woorton.com/fullchain.pem'),
    }
);

var host = config.get('host');
var port = config.get('port');
server.listen(port, host);

// Chargement de socket.io
var io = require('socket.io').listen(server);

var clients = [];

// Quand un client se connecte, on le note dans la console
io.sockets.on('connection', function (socket) {

    socket.on('storeClientInfo', function (data) {

        var clientInfo = {};
        clientInfo.customId = data.customId;
        clientInfo.clientId = socket.id;
        clients.push(clientInfo);
    });

    socket.on('disconnect', function () {

        for (var i = 0, len = clients.length; i < len; ++i) {
            var c = clients[i];

            if (c.clientId === socket.id) {
                clients.splice(i, 1);
                break;
            }
        }

    });

    socket.on('newOrder', function () {
        socket.broadcast.emit('refreshOrder');
    });

    socket.on('proposal', function (authorId) {
        socket.broadcast.emit('refreshProposal', authorId);
    });

    socket.on('cancelOrder', function (data) {
        socket.broadcast.emit('refreshCancelOrder', {authorId: data.authorId, type: data.type});
    });

    socket.on('acceptedProposal', function (data) {
        socket.broadcast.emit('acceptedRefreshProposal', {authorId: data.authorId, orderId: data.orderId});
    });

    socket.on('refusedProposal', function (data) {
        socket.broadcast.emit('refusedRefreshProposal', {authorId: data.authorId, orderId: data.orderId});
    });

    socket.on('cancelMyOrder', function (data) {
        socket.broadcast.emit('cancelFromUserOrder', {authorId: data.authorId, orderId: data.orderId});
    });

    socket.on('isUserStillHere', function (data, callback) {
        for (var i = 0, len = clients.length; i < len; ++i) {
            var c = clients[i];

            if (c.customId === data.authorId) {
                callback(true);
            }
        }
        callback(false);
    })
});

server.listen(8080);
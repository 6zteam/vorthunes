<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 02/09/18
 * Time: 14:25
 */

namespace App\Controller\Admin;

use App\Entity\Company;
use App\Form\CompanyType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AdminOrdreController
 * @package App\Controller
 *
 * @Route("/admin/company", name="admin_company_")
 */
class AdminCompanyController extends Controller
{
    /**
     * @Route("/add", name="add")
     */
    public function add(Request $request){

        $company = new Company();

        $form = $this->createForm(CompanyType::class, $company);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();

            $em->persist($company);
            $em->flush();

            $this->addFlash('success', 'Company created');
            return $this->redirectToRoute('admin_company_add');
        }

        return $this->render('admin/company/add.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/list", name="list")
     */
    public function list()
    {
        $em = $this->getDoctrine()->getManager();

        $companies = $em->getRepository(Company::class)->findAll();

        return $this->render('admin/company/list.html.twig', array(
            'companies' => $companies
        ));
    }

    /**
     * @param $id
     *
     * @Route("/delete/{id}", name="delete")
     *
     * @return Response
     */
    public function delete($id){
        $em = $this->getDoctrine()->getManager();

        $company = $em->getRepository(Company::class)->find($id);

        if(!$company->getMembers()->isEmpty()){
            $this->addFlash('danger', 'Company with members cannot be delete');
            return $this->redirectToRoute('admin_company_list');
        }

        $em->remove($company);
        $em->flush();

        return $this->redirectToRoute('admin_company_list');
    }
}
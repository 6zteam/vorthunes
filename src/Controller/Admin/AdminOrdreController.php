<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 18/07/18
 * Time: 14:22
 */

namespace App\Controller\Admin;

use App\Entity\Ordre;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminOrdreController
 * @package App\Controller
 *
 * @Route("/admin", name="admin_")
 */
class AdminOrdreController extends Controller
{

    /**
     * @Route("/ordre", name="list_ordre")
     */
    public function listOpenOrdre()
    {
        $em = $this->getDoctrine()->getManager();

        $ordres = $em->getRepository(Ordre::class)->findBy(array(
            'open' => true
        ));

        return $this->render('admin/order/open_list.html.twig', array(
            'ordres' => $ordres
        ));
    }

    /**
     * @Route("/proposal", name="proposal")
     */
    public function proposal(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $authorId = $request->request->get('authorId');
        $proposal = $request->request->get('proposal');

        $ordre = $em->getRepository(Ordre::class)->findOneBy(array('author' => $authorId, 'open' => true));
        if(!$ordre){
            return new Response("", Response::HTTP_NO_CONTENT);
        }
        $ordre->setProposal($proposal);

        $em->flush();

        return new Response("", Response::HTTP_CREATED);

    }


    /**
     * @param Request $request
     * @Route("/refresh-open-ordre", name="refresh_ordre")
     *
     * @return Response
     */
    public function refreshOpenOrdre(Request $request)
    {

        $listIds = $request->request->get('listIds');

        $em = $this->getDoctrine()->getManager();
        $missingOpenOrder = $em->getRepository(Ordre::class)->findByExludedIds($listIds);

        return new JsonResponse($missingOpenOrder);
    }

    /**
     * @param int $authorId
     * @param Request $request
     *
     * @Route("/cancel-ordre/{authorId}", name="cancel_ordre")
     *
     * @return Response
     */
    public function cancelOpenOrdre(string $authorId)
    {
       $em = $this->getDoctrine()->getManager();

       $openOrder = $em->getRepository(Ordre::class)->findOneBy(array(
           'author' => $authorId,
           'open' => true,
       ));

       $openOrder->setOpen(false);
       $openOrder->setProposalAccepted(false);
       $em->flush();

       return new Response('', Response::HTTP_ACCEPTED);
    }

    /**
     * @Route("/historic", name="historic")
     */
    public function historic(){
        $em = $this->getDoctrine()->getManager();

        $ordreRepository = $em->getRepository(Ordre::class);
        $acceptedOrders = $ordreRepository->findBy(array(
            'open' => false,
        ), array(
            'id' => 'DESC'
        ));

        return $this->render('admin/order/historic_order.html.twig', array(
            'acceptedOrders' => $acceptedOrders,
        ));
    }
}
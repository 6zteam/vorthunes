<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Form\AddUserType;
use App\Form\EditUserPasswordType;
use App\Form\EditUserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminUserController
 * @package App\Controller\Admin
 *
 * @Route("/admin/user", name="admin_user_")
 */
class AdminUserController extends Controller
{

    /**
     * @Route("/list", name="list")
     */
    public function list()
    {
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository(User::class)->findAll();

        return $this->render('admin/user/list.html.twig', array(
            'users' => $users
        ));
    }


    /**
     * @param int $id
     * @param Request $request
     *
     * @Route("/edit/{id}", name="edit")
     *
     * @return Response
     */
    public function edit(string $id, Request $request){
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository(User::class)->find($id);

        $form = $this->createForm(EditUserType::class, $user);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em->flush();
            $this->addFlash('success', 'User succefully edited');
            return $this->redirectToRoute('admin_user_list');
        }

        return $this->render('admin/user/edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @param Request $request
     *
     * @Route("/add", name="add")
     *
     * @return RedirectResponse|Response
     */
    public function add(Request $request){
        $user = new User();

        $form = $this->createForm(AddUserType::class, $user);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();

            $user->setPassword($this->get('security.password_encoder')->encodePassword($user, $user->getPassword()));
            $user->setRoles(array('ROLE_USER'));

            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'User succefully created');
            return $this->redirectToRoute('admin_user_list');
        }

        return $this->render('admin/user/add.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @param Request $request
     *
     * @Route("/edit-password/{id}", name="edit_password")
     *
     * @return Response|RedirectResponse
     */
    public function editPassword(string $id, Request $request){
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository(User::class)->find($id);

        $form = $this->createForm(EditUserPasswordType::class, $user);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){

            $user->setPassword($this->get('security.password_encoder')->encodePassword($user, $user->getPassword()));
            $user->setRoles(array('ROLE_USER'));

            $em->flush();
            $this->addFlash('success', 'Password succefully edited');
            return $this->redirectToRoute('admin_user_list');
        }

        return $this->render('admin/user/edit_password.html.twig', array(
            'form' => $form->createView(),
            'user' => $user
        ));
    }
}
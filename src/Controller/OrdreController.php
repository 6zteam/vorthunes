<?php

namespace App\Controller;

use App\Entity\Ordre;
use App\Entity\User;
use App\Form\OrderType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class OrdreController extends Controller
{
    /**
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     * @Route("/order", name="ordre")
     * @Route("/", name="ordre_index")
     */
    public function index(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $ordreRepository = $em->getRepository(Ordre::class);
        $openOrder = $ordreRepository->findOneBy(array(
            'open' => true,
            'author' => $this->getUser(),
        ));

        $data = [
            'userId' => $this->getUser()->getId(),
        ];


        if (!$openOrder || !$openOrder->isOpen()) {
            $openOrder = new Ordre();
            $openOrder->setAuthor($this->getUser());

            $form = $this->createForm(OrderType::class, $openOrder);

            $data['form'] = $form->createView();

            $form->handleRequest($request);

            if ($form && $form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($openOrder);
                $em->flush();

                unset($data['form']);
            }
        }

        $data['openorder'] = $openOrder;

        return $this->render('front/order/index.html.twig', $data);

    }

    /**
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     * @Route("/accepted-historic-order", name="accepted_historic_order")
     */
    public function acceptedHistoricOrder(){
        $em = $this->getDoctrine()->getManager();

        $ordreRepository = $em->getRepository(Ordre::class);

        $authorIds = array($this->getUser()->getId());

        if($this->getUser()->getCompany())
        {
            $authorIds = array();
            /** @var User[] $companyMembers */
            $companyMembers = $this->getUser()->getCompany()->getMembers();

            foreach ($companyMembers as $companyMember){
                $authorIds[] = $companyMember->getId();
            }
        }

        $acceptedOrders = $ordreRepository->findBy(array(
            'open' => false,
            'proposalAccepted' => true,
            'author' => $authorIds
        ), array(
            'id' => 'DESC'
        ));

        return $this->render('front/order/accepted_historic_order.html.twig', array(
            'acceptedOrders' => $acceptedOrders,
        ));
    }

    /**
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     * @Route("/accept-open-order", name="accept_ordre")
     */
    public function acceptOrdre(){
        $em = $this->getDoctrine()->getManager();

        $openOrdre = $em->getRepository(Ordre::class)->findOneBy(array(
            'open' => true,
            'author' => $this->getUser()
        ));

        if(!$openOrdre){
            return new NotFoundHttpException('No open order found');
        }

        $openOrdre->setProposalAccepted(true);
        $openOrdre->setOpen(false);
        $em->flush();

        return new Response('', Response::HTTP_ACCEPTED);
    }


    /**
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     * @Route("/refuse-open-order", name="refuse_ordre")
     */
    public function refuseOrdre(){
        $em = $this->getDoctrine()->getManager();

        $openOrdre = $em->getRepository(Ordre::class)->findOneBy(array(
            'open' => true,
            'author' => $this->getUser()
        ));

        if(!$openOrdre){
            return new NotFoundHttpException('No open order found');
        }

        $openOrdre->setProposalAccepted(false);
        $openOrdre->setOpen(false);
        $em->flush();

        return new Response('', Response::HTTP_ACCEPTED);
    }

    /**
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     * @Route("/cancel-open-order", name="cancel_ordre")
     */
    public function cancelOrdre(){
        $em = $this->getDoctrine()->getManager();

        $openOrdre = $em->getRepository(Ordre::class)->findOneBy(array(
            'open' => true,
            'author' => $this->getUser()
        ));

        if(!$openOrdre){
            return new NotFoundHttpException('No open order found');
        }

        $openOrdre->setOpen(false);
        $em->flush();

        return new Response('', Response::HTTP_ACCEPTED);
    }
}

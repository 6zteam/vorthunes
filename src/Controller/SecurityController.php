<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 18/07/18
 * Time: 17:06
 */

namespace App\Controller;


use App\Entity\User;
use Scheb\TwoFactorBundle\Security\TwoFactor\Provider\Google\GoogleAuthenticatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="security_login")
     */
    public function login(AuthenticationUtils $helper): Response
    {
        return $this->render('front/security/login.html.twig', [
            // dernier username saisi (si il y en a un)
            'last_username' => $helper->getLastUsername(),
            // La derniere erreur de connexion (si il y en a une)
            'error' => $helper->getLastAuthenticationError(),
        ]);
    }

    /**
     * La route pour se deconnecter.
     *
     * Mais celle ci ne doit jamais être executé car symfony l'interceptera avant.
     *
     *
     * @Route("/logout", name="_security_logout")
     */
    public function logout(): void
    {
        throw new \Exception('This should never be reached!');
    }

    /**
     *
     * @Route("/setup-two-factor-authentication", name="setup_two_factor")
     *
     */
    public function setUp2FA(Request $request, GoogleAuthenticatorInterface $twoFactor)
    {
        /** @var User $user */
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $postParam = $request->request;
        $verificationCode = $postParam->get('verificationCode');
        $secret = $postParam->get('secret');
        if ($verificationCode && $secret) {
            $user->setGoogleAuthenticatorSecret($secret);
            if ($twoFactor->checkCode($user, $verificationCode)) {
                $em->persist($user);
                $em->flush();

                return $this->redirectToRoute('ordre_index');
            }
        }

        $secret = $twoFactor->generateSecret();
        $user->setGoogleAuthenticatorSecret($secret);
        $qrContent = $twoFactor->getQRContent($user);


        return $this->render('front/security/set_up_two_factor.html.twig',
            [
                'secret' => $secret,
                'qrContent' => $qrContent
            ]
        );
    }
}
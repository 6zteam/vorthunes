<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 18/07/18
 * Time: 17:06
 */

namespace App\Controller;


use App\Entity\User;
use App\Form\UserChangePasswordType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController
{


    /**
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     * @Route("/change-my-password", name="change_password")
     */
    public function changePassword(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $this->getUser();
        $form = $this->createForm(UserChangePasswordType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if (!$passwordEncoder->isPasswordValid($user, $form->get('oldpassword')->getData())) {
                $this->addFlash('danger', 'Wrong old password');
                return $this->redirectToRoute('change_password');
            }

            if($form->get('password')->getData() !== $form->get('passwordRepeat')->getData()){
                $this->addFlash('danger', 'New password must be identical');
                return $this->redirectToRoute('change_password');
            }

            $user->setPassword($passwordEncoder->encodePassword($user,$form->get('password')->getData()));

            $em->flush();
            $this->addFlash('success', 'Password succefully edited');

        }

        return $this->render('front/security/change_password.html.twig', array(
            'form' => $form->createView(),
            'user' => $user
        ));
    }
}
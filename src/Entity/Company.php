<?php

namespace App\Entity;
;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

/**
 * @ORM\Entity()
 */
class Company
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @var User[]
     *
     * @ORM\OneToMany(targetEntity="App\Entity\User", mappedBy="company")
     */
    private $members;

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return PersistentCollection|Collection|User[]
     */
    public function getMembers(): ?PersistentCollection
    {
        return $this->members;
    }

    /**
     * @param User[] $members
     */
    public function setMembers(array $members): void
    {
        $this->members = $members;
    }

    public function __toString()
    {
        return $this->name;
    }
}

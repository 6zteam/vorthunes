<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrdreRepository")
 */
class Ordre
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $type;

    /**
     * @ORM\Column(type="decimal", scale=6)
     */
    private $amount;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $pair;
    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     */
    private $open = true;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="orders")
     */
    private $author;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $proposal = null;


    /**
     * Set when proposal is set
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     *
     */
    private $proposalDate;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default" : 0})
     */
    private $proposalAccepted = false;

    public function getId()
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getPair(): ?string
    {
        return $this->pair;
    }

    /**
     * @param mixed $pair
     */
    public function setPair($pair): void
    {
        $this->pair = $pair;
    }



    public function isOpen(): bool
    {
        return $this->open;
    }

    /**
     * @param mixed $open
     */
    public function setOpen($open): void
    {
        $this->open = $open;
    }

    /**
     * @return User
     */
    public function getAuthor(): ?User
    {
        return $this->author;
    }

    /**
     * @param User $author
     */
    public function setAuthor(User $author): void
    {
        $this->author = $author;
    }

    /**
     * @return float
     */
    public function getProposal(): ?float
    {
        return $this->proposal;
    }

    /**
     * @param float $proposal
     */
    public function setProposal(float $proposal): void
    {
        $this->proposal = $proposal;
        $this->proposalDate = new \DateTime();
    }

    /**
     * @return bool
     */
    public function isProposalAccepted(): bool
    {
        return $this->proposalAccepted;
    }

    /**
     * @param bool $proposalAccepted
     */
    public function setProposalAccepted(bool $proposalAccepted): void
    {
        $this->proposalAccepted = $proposalAccepted;
    }

    /**
     * @return \DateTime|null
     */
    public function getProposalDate(): ?\DateTime
    {
        return $this->proposalDate;
    }





}

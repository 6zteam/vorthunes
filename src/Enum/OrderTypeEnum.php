<?php

namespace App\Enum;

class OrderTypeEnum extends AbstractEnum
{
    public const SELL = 'sell';
    public const BUY = 'buy';

    public static function toArray()
    {
        return array(
            'BUY' => self::BUY,
            'SELL' => self::SELL,
        );
    }
}
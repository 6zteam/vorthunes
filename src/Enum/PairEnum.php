<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 08/07/18
 * Time: 16:19
 */

namespace App\Enum;


class PairEnum extends AbstractEnum
{
    public const BTCEUR = "BTCEUR";
    public const ETHEUR = "ETHEUR";
    public const LTCEUR = "LTCEUR";
    public const BCHEUR = "BCHEUR";
    public const XRPEUR = "XRPEUR";
    public const BTCUSD = "BTCUSD";
    public const ETHUSD = "ETHUSD";
    public const LTCUSD = "LTCUSD";
    public const BCHUSD = "BCHUSD";
    public const XRPUSD = "XRPUSD";

    public static function toArray()
    {
        return array(
            'BTCEUR' => self::BTCEUR,
            'ETHEUR' => self::ETHEUR,
            'LTCEUR' => self::LTCEUR,
            'BCHEUR' => self::BCHEUR,
            'XRPEUR' => self::XRPEUR,
            'BTCUSD' => self::BTCUSD,
            'ETHUSD' => self::ETHUSD,
            'LTCUSD' => self::LTCUSD,
            'BCHUSD' => self::BCHUSD,
            'XRPUSD' => self::XRPUSD,
        );
    }
}
<?php

namespace App\EventListener;


use App\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class DefaultListener
{

    private $router;
    private $token;

    public function __construct(Router $router, TokenStorage $tokenStorage)
    {
        $this->router = $router;
        $this->token = $tokenStorage;
    }

    /**
     * @param GetResponseEvent $event
     * @return RedirectResponse|void
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return null;
        }

        if ("/setup-two-factor-authentication" === $event->getRequest()->getRequestUri()) {
            return null;
        }
        /** @var User $user */
        if ($token = $this->token->getToken()) {
            $user = $token->getUser();
            if ($user && "anon." !== $user) {
                if (!$user->isGoogleAuthenticatorEnabled()) {
                    $url = $this->router->generate('setup_two_factor');
                    $event->setResponse($response = new RedirectResponse($url));
                }

            }
        }
    }
}
<?php

namespace App\Form;

use App\Entity\Company;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, array(
                'label' => 'Username'
            ))
            ->add('email', EmailType::class, array(
                'label' => 'Email'
            ))
            ->add('password', PasswordType::class, array(
                'label' => 'Password'
            ))
            ->add('fullName', TextType::class, array(
                'label' => 'Full name'
            ))
            ->add('company', EntityType::class, array(
                'class' => Company::class,
                'label' => "Company",
                'required' => false,
                'placeholder' => "Choose a company"
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class
        ));
    }
}
<?php

namespace App\Form;

use App\Entity\Ordre;
use App\Enum\OrderTypeEnum;
use App\Enum\PairEnum;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', ChoiceType::class, array(
                'choices' => OrderTypeEnum::toArray(),
                'label' => "Direction"
            ))
            ->add('pair', ChoiceType::class, array(
                'choices' => PairEnum::toArray(),
                'label' => "Pair"
            ))
            ->add('amount', NumberType::class, array(
                'label' => "Amount",
                'scale' => 6
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Ordre::class
        ));
    }
}
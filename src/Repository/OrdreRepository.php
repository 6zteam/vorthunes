<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 18/07/18
 * Time: 15:33
 */

namespace App\Repository;


use Doctrine\ORM\EntityRepository;

class OrdreRepository extends EntityRepository
{
    public function findByExludedIds(?array $listIds){

        $query = 'SELECT 
                    o.id,
                    o.amount,
                    o.pair,
                    o.type,
                    u.full_name,
                    u.id authorId
                  FROM ordre o
                  INNER JOIN user u ON u.id = o.author_id 
                  WHERE o.open = 1';

        if($listIds){
            $query .= ' AND o.id NOT IN ('.implode(',', $listIds).')';
        }

        $stmt = $this->getEntityManager()->getConnection()->prepare($query);
        $stmt->execute();

        return $stmt->fetchAll();
    }
}